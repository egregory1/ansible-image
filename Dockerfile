FROM redhat/ubi8:latest

USER root

RUN yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
RUN yum install -y yum-utils
RUN yum-config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
RUN yum repolist
RUN /usr/bin/crb enable
RUN yum -y install procps-ng
RUN yum -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
RUN yum install python3 -y
RUN alternatives --set python /usr/bin/python3
RUN python -m pip install --upgrade pip
RUN pip3 install ansible 
